
## Installation et configuration d'un serveur Web sous Docker

Vous allez faire tourner un serveur Web sur votre machine grâce à Docker. 

### Installer Docker et Docker Desktop

Sur les ordinateurs portables fournis par l'IUT, Docker et son interface
graphique Docker Desktop sont déjà installés.

Pour les élèves qui travaillent sur leur ordinateur personnel, il faut installer ces logiciels :

* MacOs : [Install Docker Desktop on Mac](https://docs.docker.com/desktop/install/mac-install/)
* Windows : [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install/)
* Linux : 
  * Les instructions générales se trouvent sur le lien [Install Docker Desktop on Linux](https://docs.docker.com/desktop/install/linux-install/).
  * Dans le cas de la distribution Ubuntu ≥ 22.04, voici un résumé des [instructions d'installation spécifiques](https://docs.docker.com/desktop/install/ubuntu/).
    * Mettez en place le dépôt logiciel de Docker 
      ```bash
      # Add Docker's official GPG key:
      sudo apt-get update
      sudo apt-get install ca-certificates curl
      sudo install -m 0755 -d /etc/apt/keyrings
      sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
      sudo chmod a+r /etc/apt/keyrings/docker.asc
      
      # Add the repository to Apt sources:
      echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
      sudo apt-get update
      ```
    * Téléchargez le dernier [*paquet DEB*](https://desktop.docker.com/linux/main/amd64/docker-desktop-amd64.deb?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-linux-amd64).
    * Installez ce *paquet DEB* avec *apt* comme suit, en replaçant `<arch>` :
      ```bash
      sudo apt-get update
      sudo apt-get install ./docker-desktop-<arch>.deb
      ```
    * Si vous avez **Ubuntu 24.04** et que vous n'êtes pas sur les portables fournis par l'IUT, exécutez la commande suivante à chaque redémarrage du système afin que Docker Desktop puisse se lancer 
      ```bash
      sudo sysctl -w kernel.apparmor_restrict_unprivileged_userns=0
      ```

      **Astuce :** Si vous ne voulez pas exécuter de commande à chaque redémarrage, vous pouvez rajouter la ligne suivante à `/etc/sysctl.conf`
      ```
      kernel.apparmor_restrict_unprivileged_userns=0
      ```
      Cette manipulation a déjà été faite sur les portables de l'IUT.



#### Lancer Docker Desktop

Vous pouvez alors lancer Docker Desktop et obtenir la première fenêtre d'accueil

![WelcomeToDocker](./img/WelcomeToDocker.png){: .blockcenter}

Cliquez sur *Skip* 2 fois jusqu'à arriver à l'interface principale

![DockerEmptyContainers](./img/DockerEmptyContainers.png){: .blockcenter}

### Construire l'image du serveur Web

Nous vous fournissons la configuration Docker pour créer votre serveur Web : 

1. Clonez ce dépôt Git
   ```bash
   git clone https://gitlabinfo.iutmontp.univ-montp2.fr/Enseignants-Web/docker.git serveurWebDocker
   ```
2. Entrez dans le dépôt Git ainsi créé
   ```bash
   cd serveurWebDocker
   ```
3. Construisez l'image du serveur Web
   ```bash
   docker build --tag serveur.web.docker.iut .
   ```

   *Note* : 
   * La construction peut prendre quelques minutes.
   * L'option *tag* donne le nom *serveur.web.docker.iut* à votre image.
   * Osons une analogie entre l'exécution d'un programme et l'exécution d'une machine virtuelle avec Docker.
     * Le fichier *Dockerfile* du dépôt Git fourni comme *le code source* de la machine virtuelle que l'on veut créer
     * Une image Docker serait l'équivalent d'un programme. Autrement dit, une image Docker est un peu comme une machine virtuelle codée dans un langage bas niveau.
     * La construction d'une image Docker est un peu comme la compilation d'un programme à partir de son code source. 

4. L'image construite doit désormais apparaître sous Docker Desktop dans l'onglet de gauche *Images*

   ![ImageServeurCreee](./img/ImageServeurCreee.png){: .blockcenter}

### Créer le répertoire qui contiendra les pages Web

Votre futur serveur Web a besoin d'un dossier qui contiendra toutes vos pages
Web. **Copiez-collez** le dossier `public_html` de ce dépôt Git à la racine de votre répertoire personnel. 

Le dossier `public_html` que nous fournissons contient 2 fichiers : 
* `index.html` : pour afficher un message de succès quand vous testerez votre
  serveur à la prochaine étape.
* `.htaccess` : fichier de configuration du serveur Web Apache, qui active
  l'affichage des dossiers dans notre cas.

### Lancer un serveur Web en tant que conteneur Docker 

Il est temps de lancer notre serveur Web : 

1. Dans l'onglet de gauche *Images*, cliquez sur le bouton *Play* ![BoutonPlay](./img/BoutonPlay.png) pour lancer l'image *serveur.web.docker.iut* 

   ![ImageServeurCreee](./img/ImageServeurCreee.png){: .blockcenter}

2. Remplissez les *Optional settings* comme suit : 
   1. Nom du conteneur / *Container name* : serveurWebIUT1
   2. Port de la machine hôte / *Host port* : 80
   3. Dans la partie *Volumes*,  
      *Host path* : chemin de fichier jusqu'au répertoire `public_html`  
      *Container path* : `/var/www/html`  
      ![OptionalSettings](./img/OptionalSettings.png){: .blockcenter}

3. Cliquez sur *Run* et votre serveur se lance  
   ![ServeurLance](./img/ServeurLance.png){: .blockcenter}
4. Vous pouvez tester que votre serveur marche en cliquant sur le lien `80:80`
   dans la fenêtre Docker précédente, ou en ouvrant un navigateur Web à l'URL
   [`http://localhost`](http://localhost).

   La page suivante doit s'afficher 

   ![SuccesServeurWeb](img/SuccesServeurWeb.png)

1. Vous pouvez retrouver votre serveur Web dans l'onglet Gauche *Containers* de Docker Desktop.
  C'est ici qu'il faudra relancer votre conteneur à chaque redémarrage de votre machine à l'aide du bouton *Play* ![BoutonPlay](./img/BoutonPlay.png).  
  ![ConteneurArrete](./img/ConteneurArrete.png)

Notes : 
* Docker fait tourner votre serveur Web sur une machine virtuelle. C'est un peu
  comme si votre serveur Web tournait simultanément sur votre machine sur un
  système d'exploitation parallèle. 
* Pour poursuivre l'analogie précédente entre l'exécution d'un programme et l'exécution d'une machine virtuelle avec Docker : 
  * un conteneur est un peu comme l'équivalent d'un processus, c'est-à-dire
    qu'un conteneur est une exécution d'une image là où un processus est une
    exécution d'un programme. 
* Les parcours A et B étudieront le fonctionnement de Docker plus en profondeur
dans les cours "Virtualisation" (semestre 4) et "Virtualisation avancée" au
semestre 5.


## Accélérer Docker sous Windows

Docker peut tourner au ralenti sur Windows si le site web utilise beaucoup de fichier (avec Symfony par exemple).
La solution est :

1. Installer une distribution wsl ubuntu (ou autre) avec la commande `wsl --install` exécutée depuis le PowerShell.
2. Je conseille ensuite de switcher l'utilisateur de ce système en root (pour ne pas avoir de problèmes de permissions si des fichiers sont créées à l'intérieur du conteneur). Depuis le PowerShell : 
    ```
    ubuntu config --default-user root
    ```
3. Sur docker desktop, il faut aller dans la configuration : _configuration_ → _ressources_ → _wsl integrations_ puis cocher la case ubuntu, puis "_apply & restart_".
4. Enfin, le conteneur devra être créé et exécuté à l'intérieur du wsl ubuntu.